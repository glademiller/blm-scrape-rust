// Copyright 2014 The html5ever Project Developers. See the
// COPYRIGHT file at the top-level directory of this distribution.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

extern crate url;
extern crate http;
extern crate html5ever;
extern crate serialize;

use serialize::json;


use std::string::String;
use std::default::Default;
use std::comm::channel;

use std::collections::HashMap;

use url::Url;

use html5ever::sink::common::{Text, Element, Document, Doctype, Comment};
use html5ever::sink::rcdom::{RcDom, Handle};
use html5ever::{parse, one_input};

use http::client::request::{RequestWriter};
use http::method::Get;


fn walk(indent: uint, handle: Handle) {
    let node = handle.borrow();
    // FIXME: don't allocate
    print!("{:s}", String::from_char(indent, ' '));
    match node.node {
        Document
            => println!("#Document"),

        Doctype(ref name, ref public, ref system)
            => println!("<!DOCTYPE {:s} \"{:s}\" \"{:s}\">", *name, *public, *system),

        Text(ref text)
            => println!("#text: {:s}", text.escape_default()),

        Comment(ref text)
            => println!("<!-- {:s} -->", text.escape_default()),

        Element(ref name, ref attrs) => {
            print!("<{:s}", name.as_slice());
            for attr in attrs.iter() {
                print!(" {:s}=\"{:s}\"", attr.name.name.as_slice(), attr.value);
            }
            println!(">");
        }
    }

    for child in node.children.iter() {
        walk(indent+4, child.clone());
    }
}


fn get_attr_value(handle: Handle, attr_name: &str) -> Option<String> {
    let node = handle.borrow();
    match node.node {
        Element(_, ref attrs) => {
            for attr in attrs.iter() {
                if attr.name.name.as_slice() == attr_name {
                    return Some(attr.clone().value);
                }
            }
            return None
        },
        _ => return None
    }
}

fn get_text(handle: Handle) -> Option<String> {
    let text_nodes = query(handle, "text");
    let first = text_nodes[0].borrow();
    match first.node {
        Text(ref text)
            => Some(text.escape_default()),
        _ => None
    }
}

fn query(handle: Handle, tag_name: &str) -> Vec<Handle> {
    let mut nodes: Vec<Handle> = Vec::new();
    let node = handle.borrow();
    match node.node {
        Text(_) => {
            if tag_name == "text" {
                nodes.push(handle.clone());
            }
        },
        Element(ref name, _) => {
            if name.as_slice() == tag_name {
                nodes.push(handle.clone());
            }
        },
        _ => {},
    }

    for child in node.children.iter() {
        nodes = nodes.clone().append(query(child.clone(), tag_name).as_slice());
    }
    nodes
}

fn get_page(uri: &str) -> String {
    let url = Url::parse(uri).unwrap();
    let request: RequestWriter = match RequestWriter::new(Get, url) {
        Ok(request) => request,
        Err(error) => fail!("{}", error),
    };
    let mut response = match request.read_response() {
        Ok(response) => response,
        Err((_request, error)) => fail!("{}", error),
    };
    let body = match response.read_to_string() {
        Ok(body) => body,
        Err(error) => fail!("Failed to read response body {}", error),
    };
    body
}

#[deriving(Show, Decodable, Encodable, Clone)]
struct Location {
    name: String,
    url: String,
    horses: Vec<Horse>
}

impl Location {
    fn new(name: String, url: String) -> Location {
        Location {
            name: name, 
            url: url.clone(), 
            horses: get_horses(url.clone())
        }
    }
}

type Horse = HashMap<String, String>;

fn get_horses(url: String) -> Vec<Horse> {
    let location_body = get_page(url.as_slice());
    let dom: RcDom = parse(one_input(location_body), Default::default());
    let handle = dom.document;
    let mut horses: Vec<Horse> = Vec::new();
    let all_tables = query(handle, "table");
    let mut tables = all_tables.iter().filter(|table| match get_attr_value(table.clone().clone(), "class") {
        Some(val) => "gal".to_string() == val,
        None => false
    });
    for table in tables {
        match get_horse(table.clone()) {
            Some(horse) => horses.push(horse),
            None => {}
        }
    }
    horses
}

fn get_horse(handle: Handle) -> Option<Horse> {
    let mut horse: Horse = HashMap::new();
    let links = query(handle.clone(), "a");
    if links.len() == 0 {
        return None;
    }
    match get_attr_value(links.index(&0).clone(), "href") {
        Some(link) => {
            horse.insert("url".to_string(), "https://www.blm.gov/adoptahorse/".to_string() + link);
        },
        None => {
            return None;
        }
    };

    let pars = query(handle.clone(), "p");
    for par in pars.iter() {
        let mut attr_name = String::new();
        for child in par.borrow().children.iter() {
            match child.borrow().node {
                Text(ref val) => {
                    if attr_name != "".to_string() && val.as_slice().trim().len() > 0 {
                        horse.insert(attr_name.clone(), val.as_slice().trim().to_string());
                    }
                    attr_name = String::new();
                },
                Element(ref name, _) => {
                    if name.as_slice() == "strong" {
                        match get_text(child.clone()) {
                            Some(txt) => {
                                attr_name = txt.as_slice().trim().slice(0, txt.len() - 1).to_string();
                            },
                            None => {
                                return None;
                            }
                        }
                    }
                },
                _ => {}
            }
        }
    }

    Some(horse)
}

fn get_locations(body: String) -> Vec<Location> {
    let dom: RcDom = parse(one_input(body), Default::default());
    let h4s = query(dom.document, "h4");
    let mut links = h4s.iter().map(|h4| query(h4.clone(), "a")).map(|href| href.index(&0).clone());
    let (tx, rx) = channel::<Location>();
    let mut link_count = 0;
    for link in links {
        match (get_attr_value(link.clone(), "href"),
                get_text(link.clone())) {
            (Some(url), Some(txt)) => {
                link_count = link_count + 1;
                let absolute_url = "https://www.blm.gov/adoptahorse/".to_string() + url;
                let tx = tx.clone();
                spawn(proc () {
                    tx.send(Location::new(txt, absolute_url.clone()));
                });
            },
            (None, _) => {},
            (_, None) => {}
        }
    }

    let mut locations: Vec<Location> = Vec::new();
    for _ in range(0u, link_count) {
        let loc = rx.recv();
        locations.push(loc);
    }
    locations
}

fn main() {
   let body = get_page("https://www.blm.gov/adoptahorse/onlinegallery.php");
   let locations = get_locations(body);
   let encoded = json::encode(&locations);
   println!("{}", encoded)
}
